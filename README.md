# Authoring Tools

This is a collection of various Rock Band authoring tools that you can use for custom songs and to modify existing official DLC. These tools include:

* **ArkTools.** ArkTools by xorloser allows one to open ARK files on various Rock Band discs in order to import the songs to other Rock Band titles.
* **OGG2MOGG.exe.** This executable by DarkMatterCore converts a standard OGG file into a multi-track OGG file that's compatible with Rock Band.
* **MiloMod.** StackOverflow0x's MiloMod program can edit and convert a MILO file, as well as manipulate a corresponding MIDI, to enhance the lip sync effects and animations in Rock Band titles.
